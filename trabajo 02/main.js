

const app = new Vue({
    el: '#app',
    data:{
        titulo: 'Pablo Joel Tulian Vue JS !!',
        tareas: [
            {
                tarea: 'Lavar ropa',
                duracion: '20'
            },
            {
                tarea: 'almorzar',
                duracion: '45'
            },
            {
                tarea: 'Estudiar',
                duracion: '120'
            },
            {
                tarea: 'pasear al perro',
                duracion: '35'
            }
        ],
        tareasNueva: {
            nombre:'',
            duracion:0
        },
        totalMinutos: 0
    },
    methods:{
        agregartarea () {
            this.tareas.push({
                tarea: this.tareasNueva.tarea,
                duracion: this.tareasNueva.duracion 
            });
            this.tareasNueva.tarea = "";
            this.tareasNueva.duracion= 0;
        }
    },
    computed: {
        calcularTotalMinutos () {
            this.totalMinutos = 0;
            for(let itemTarea of this.tareas){
                this.totalMinutos = parseInt(this.totalMinutos) + parseInt(itemTarea.duracion);
            }
            return this.totalMinutos;
        }
    }
});