let personasObj = [
    {
        id:1,
        nombre:"Pablo",
        email:"pablotulian10@gmail.com",
        edad:10,
        estado:true,
        createAt:new Date().toLocaleString()
    },
    {   
        id:2,
        nombre:"leonardo",
        email:"leonardo@gmail.com",
        edad:10,
        estado:false,
        createAt:new Date().toLocaleString()
    },
    {   
        id:3,
        nombre:"Brinito",
        email:"Brinito@gmail.com",
        edad:10,
        estado:true,
        createAt:new Date().toLocaleString()
    },
    {   
        id:4,
        nombre:"Micaela",
        email:"Micaela@gmail.com",
        edad:10,
        estado:false,
        createAt:new Date().toLocaleString()
    }
];

 
const app = new Vue({
    el: '#app',
    data:{
        titulo: 'Hola mundo Con Vue JS',
        personas: personasObj,
        message: 'You loaded this page on ' + new Date().toLocaleString()
    }
});