
const vueJS = new Vue({
    el:'#app',
    data:{
        titulo: 'Pablo Joel Tulian - Vue JS',
        tareas: [],
        nuevaTarea: ''
    },
    methods:{
        agregarTarea() {
            this.tareas.push({
                nombre: this.nuevaTarea,
                estado: false 
            });
            this.nuevaTarea = '';
            localStorage.setItem('gym-vue',JSON.stringify(this.tareas));
        },
        cambiarEstado(index){
            this.tareas[index].estado =  !this.tareas[index].estado;
            localStorage.setItem('gym-vue',JSON.stringify(this.tareas));
        },
        eliminarItem(index){
            this.tareas.splice(index,1);
            localStorage.setItem('gym-vue',JSON.stringify(this.tareas));
        }
    },
    computed:{
        
    },
    created: function(){
        let datosDB = JSON.parse(localStorage.getItem('gym-vue'));
        if (datosDB===null) {
            this.tareas = [];
        }else{
            this.tareas = datosDB;
        }
    }

});