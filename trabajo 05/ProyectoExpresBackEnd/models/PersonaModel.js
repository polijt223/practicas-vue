import mongoose, {Schema} from 'mongoose';

const personaSchema = new Schema({
    nombre:{type:String, maxlength:20, required:true },
    apellido:{type:String, maxlength:20, required:true},
    mail:{type:String, maxlength:35, required:true, unique:true},
    edad:{type:Number, required:true},
    documento:{type:Schema.ObjectId, ref:'documento' ,required:true},
    createAt:{type:Date,default:Date.now},
    estado:{type:Number,default:1},
    direccion:[
        {
        calle:{type:String,required:true},
        numeracion:{type:Number,required:true},
        barrio:{type:String , required:true},
        pais:{type:String,required:true}
        }
    ]
});

const PersonaModel = mongoose.model('persona',personaSchema);

export default PersonaModel;