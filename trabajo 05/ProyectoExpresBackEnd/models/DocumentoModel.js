import mongoose, {Schema} from 'mongoose';

const documentoSchema = new Schema({
    tipo_documento:{type:String,required:true,unique:true}
});

const DocumentoModel = mongoose.model('documento',documentoSchema);

export default DocumentoModel;