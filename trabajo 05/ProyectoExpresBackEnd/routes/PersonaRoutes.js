import routesx from 'express-promise-router';
import PersonaController from '../controllers/PersonaController';

const PersonaRoutes = routesx();

PersonaRoutes.post('/add',PersonaController.add);
PersonaRoutes.get('/query',PersonaController.query);
PersonaRoutes.get('/list',PersonaController.list);
PersonaRoutes.post('/delete',PersonaController.delete);
PersonaRoutes.put('/update',PersonaController.update);

export default PersonaRoutes;