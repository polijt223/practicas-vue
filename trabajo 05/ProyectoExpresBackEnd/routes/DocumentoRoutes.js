import routesx from 'express-promise-router';
import DocumentoController from '../controllers/DocumentoController'

const DocumentoRoutes = routesx();

DocumentoRoutes.post('/add',DocumentoController.add);
DocumentoRoutes.get('/query',DocumentoController.query);
DocumentoRoutes.get('/list',DocumentoController.list);

export default DocumentoRoutes;