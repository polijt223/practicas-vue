import routex from 'express-promise-router';
import DocumentoRoutes from './DocumentoRoutes';
import PersonaRoutes from './PersonaRoutes';

const routes = routex();

routes.use('/documento',DocumentoRoutes);
routes.use('/persona',PersonaRoutes);

export default routes;