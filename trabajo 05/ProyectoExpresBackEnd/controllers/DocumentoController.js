import models from '../models';

export default {
    
    add: async(req,res,next) => {
        try {
            const reg = await models.DocumentoModel.create(req.body);
            res.status(200).send({
                mensaje:'Se creo correctamente el tipo de documento',
                docuemto: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al crear el nuevo tipo de documento',
                error: e
            });
            next(e);
        }
    },

    query: async(req,res,next) => {
        try {
            const reg = await models.DocumentoModel.findOne({_id:req.query._id});
            res.status(200).send({
                mensaje:'Se encontro correctamente el tipo de documento',
                docuemto: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al encontrar el nuevo tipo de documento',
                error: e
            });
            next(e);
        }
    },
    
    list: async(req,res,next) => {
        try {
            const reg = await models.DocumentoModel.find();
            res.status(200).send({
                mensaje:'Se encontro correctamente el tipo de documento',
                docuemto: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al encontrar el nuevo tipo de documento',
                error: e
            });
            next(e);
        }
    }
  
    



}