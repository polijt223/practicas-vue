import models from '../models';

export default{

    add: async(req,res,next) => {
        try {
            const reg = await models.PersonaModel.create(req.body);
            res.status(200).send({
                mensaje:'Se creo correctamente',
                persona: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al crear',
                error: e
            });
            next(e);
        }
    },

    query: async(req,res,next) => {
        try {
            const reg = await models.PersonaModel.findOne({_id:req.query._id})
            .populate('documento',{'tipo_documento':1});
            res.status(200).send({
                mensaje:'Se encontro correctamente',
                persona: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al encontrar',
                error: e
            });
            next(e);
        }
    },
    
    list: async(req,res,next) => {
        try {
            const reg = await models.PersonaModel.find()
            .populate('documento');
            res.status(200).send({
                mensaje:'Se encontro correctamente',
                persona: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al encontrar',
                error: e
            });
            next(e);
        }
    },
    delete: async(req,res,next) => {
        try {
            const reg = await models.PersonaModel.findOneAndRemove({_id: req.body._id});
            res.status(200).send({
                mensaje:'Se elimino correctamente al usuario',
                persona: reg
            })
        } catch (e) {
            res.status(500).send({
                mensaje:'Error a al crear el nuevo tipo de documento',
                error: e
            });
            next(e);
        }
    },
    update: async(req,res,next) =>{
        try{
            const reg = await models.PersonaModel.findByIdAndUpdate({_id:req.body._id},
                {
                    nombre:req.body.nombre,
                    apellido:req.body.apellido,
                    edad:req.body.edad,
                    mail:req.body.mail,
                    direccion:req.body.direccion, 
                    documento:req.body.documento,  
                });
            res.status(200).json(reg);
        }catch(e){
            res.status(500).send({
                message: 'Ocurrio un error' 
            });
            next(e);
        }
    }



}