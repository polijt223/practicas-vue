const app = new Vue({
    el: "#app",
    data:{
        titulo:'Pablo Joel Tulian - Vue JS',
        users: [],
        mensajeResultado:'',
        modalTitle:'',
        createUpdate:true,
        persona: {
            nombre:'',
            apellido:'',
            mail:'',
            edad:0,
            documento:'',
            direccion:{
                calle:'',
                numeracion:0,
                barrio:'',
                pais:''
            }
        },personaReset: {
            nombre:'',
            apellido:'',
            mail:'',
            edad:0,
            documento:'',
            direccion:{
                calle:'',
                numeracion:0,
                barrio:'',
                pais:''
            }
        }
    },
    methods:{
        eliminar (__id){
            if(confirm("Deseas eliminar el registro")){
                let persona = {_id: __id}
                axios.post("http://localhost:3000/api/persona/delete" , persona)
                .then( response =>{
                    this.mensajeResultado = response.data.mensaje;
                    console.log(this.mensajeResultado);
                });
            }
            this.listar();
        },
        mostrarActulizar(user){
            
            this.persona.nombre = user.nombre;
            this.persona.apellido = user.apellido;
            this.persona.edad = user.edad;
            this.persona.mail = user.mail;
            this.persona.direccion = user.direccion[0];
            this.persona._id = user._id;

            if (user.documento._id == '5d71eae63f365633dca0ed22'){ this.persona.documento = '1' }
            if (user.documento._id == '5d71eac03f365633dca0ed1f'){ this.persona.documento = '2' }
            if (user.documento._id == '5d71eade3f365633dca0ed21'){ this.persona.documento = '3' }
            if (user.documento._id == '5d71ead63f365633dca0ed20'){ this.persona.documento = '4' }

            this.modalTitle = 'Editar Usuario';
            this.createUpdate = false;

        },
        mostrarCrear(){
            this.modalTitle = 'Crear Usuario';
            this.createUpdate = true;
        },
        createOrUpdate(){
            if (!this.createUpdate) {
                this.updatePersona();      
            } else {
                this.crearPersona();
            }
            this.persona = this.personaReset;
            this.listar();
        },
        crearPersona(){
            
            if (this.persona.documento==1) { this.persona.documento = '5d71eae63f365633dca0ed22'; }
            if (this.persona.documento==2) { this.persona.documento = '5d71eac03f365633dca0ed1f'; }
            if (this.persona.documento==3) { this.persona.documento = '5d71eade3f365633dca0ed21'; }
            if (this.persona.documento==4) { this.persona.documento = '5d71ead63f365633dca0ed20'; }

            this.persona.direccion.numeracion = parseInt(this.persona.direccion.numeracion);
            this.persona.edad = parseInt(this.persona.edad);
            console.log(this.persona);
            
            axios.post("http://localhost:3000/api/persona/add" , this.persona)
            .then( response =>{
                this.mensajeResultado = response.data.mensaje;
                alert(this.mensajeResultado);
            });
            
            $('#ModalPersona').modal('hide');
            this.listar();

        },
        updatePersona(){
            if (confirm("Seguro desea actualizar el registro?")) {
                console.log("Se actualiza registro");
                if (this.persona.documento==1) { this.persona.documento = '5d71eae63f365633dca0ed22'; }
                if (this.persona.documento==2) { this.persona.documento = '5d71eac03f365633dca0ed1f'; }
                if (this.persona.documento==3) { this.persona.documento = '5d71eade3f365633dca0ed21'; }
                if (this.persona.documento==4) { this.persona.documento = '5d71ead63f365633dca0ed20'; }

                this.persona.direccion.numeracion = parseInt(this.persona.direccion.numeracion);
                this.persona.edad = parseInt(this.persona.edad);
                console.log(this.persona);
                  
                axios.put("http://localhost:3000/api/persona/update" , this.persona)
                .then( response =>{
                    this.mensajeResultado = response.data.mensaje;
                    alert(this.mensajeResultado);
                });
                
                $('#ModalPersona').modal('hide');
                
                
            }
        },
        listar(){
            axios.get("http://localhost:3000/api/persona/list")
            .then(response =>{
                this.users = response.data.persona;
            });  
        }

    },
    computed:{
        
    },
    created: 
        function(){
            axios.get("http://localhost:3000/api/persona/list")
            .then(response =>{
                this.users = response.data.persona;
            });   
        }
    
});