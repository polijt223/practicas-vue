const app = new Vue({
    el:"#app",
    data:{
        titulo:'Pablo Joel Tulian - Vue JS',
        contador:0
    },
    computed:{
        color(){
            return{
                'bg-success': this.contador<=10,
                'bg-warning': this.contador>10 && this.contador<=20,
                'bg-dark': this.contador>20 && this.contador<=30,
                'bg-secondary': this.contador>30 && this.contador<=40,
                'bg-primary': this.contador>40 && this.contador<=50                
            }
        }
    }

});