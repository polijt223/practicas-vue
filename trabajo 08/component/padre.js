Vue.component('padre', {
    template: //html
    `
    <div class="p-5 bg-dark text-white">  
        <h1>{{numeroPadre}}  </h1> 
        <button class="btn btn-primary" @click="numeroPadre++" >+</button> 
        <button class="btn btn-danger" @click="numeroPadre--" >-</button> 
        <div class="form-group">
          <label for="">{{nombrePadre}}</label><br>
          <label for="">{{nombreHijo}}</label>
          <input type="text" class="form-control" name="" id="" v-model="nombrePadre" aria-describedby="helpId" placeholder="">
        </div>
        <hijo :nombrePadre="nombrePadre" :numeroPadre="numeroPadre" @textoHijoEnviado=" nombreHijo = $event" @numeroHijoEnviado=" numeroPadre = $event"  ></hijo>
        <hijo :nombrePadre="nombrePadre" :numeroPadre="numeroPadre" @textoHijoEnviado=" nombreHijo = $event" @numeroHijoEnviado=" numeroPadre = $event"  ></hijo>
    </div>
    `,
    data(){
        return{
            nombrePadre:"",
            nombreHijo:"",
            numeroPadre:0
        }
    }
});