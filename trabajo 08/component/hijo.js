Vue.component('hijo', {
    template: //html
    `
    <div class="p-5 bg-success text-white">        
        <h5>{{nombrePadre}}</h5>  
        <h5>{{asignarNombreHijo}}</h5>  
        <h5>{{nombreHijo}}</h5> 
        <button class="btn btn-primary" @click="sumar" >+</button>
        <button class="btn btn-danger" @click="restar" >-</button>
        <button class="btn btn-warning" @click="reset" >C/</button>
        <h4>{{ numeroPadre }}</h4>
    </div>
    `,
    props: ['nombrePadre','numeroPadre'],
    data(){
        return{
            nombreHijo:'',
            numeroHijo:0,
        }
    },
    computed:{
        asignarNombreHijo: function(){
            this.nombreHijo=this.nombrePadre;
            this.$emit('textoHijoEnviado',this.nombrePadre);
            return this.nombreHijo;
        }      
    },
    methods:{
        sumar: function(){
            this.numeroHijo = this.numeroPadre +1 ;
            this.$emit('numeroHijoEnviado',this.numeroHijo)
        },
        restar: function(){
            this.numeroHijo = this.numeroPadre -1 ;
            this.$emit('numeroHijoEnviado',this.numeroHijo)
        },
        reset: function(){
            this.numeroHijo = 0 ;
            this.$emit('numeroHijoEnviado',this.numeroHijo)
        }

    }
});