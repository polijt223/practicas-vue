
const app = new Vue({
    el: '#app',
    data: {
        pokemones: [],
        getUrlPokemon:[],
        linkPokemon: [],
        pagActual: 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=12'
    },
    methods:{
        pagNext: function(){
            if (this.getUrlPokemon.next!=null) {
                this.pagActual = this.getUrlPokemon.next;
                this.pokemones = [];
                this.getUrlPokemon  = [];
                this.linkPokemon  = [];
                this.actualizarLista();
            }           
            
        },
        pagPrev: function(){
            if (this.getUrlPokemon.previous!=null) {
                this.pagActual = this.getUrlPokemon.previous;
                this.pokemones = [];
                this.getUrlPokemon  = [];
                this.linkPokemon  = [];
                this.actualizarLista(); 
            }           
            
        },
        actualizarLista: async function () {
            try {
                let response =  await axios.get(this.pagActual);
                this.getUrlPokemon = response.data;
                for (let item of this.getUrlPokemon.results) {
                    this.linkPokemon.push({
                    url: item.url
                    })
                }
				let pokecache = [];
                for (let item of this.linkPokemon) {
                    let responseLinkPokemon = await axios.get(item.url);
                    pokecache.push({
                        pokemon: responseLinkPokemon.data
                    });
                }
				this.pokemones = pokecache;
            }catch (e) {
                console.log(e);
            }
        }
    },
    created: function() {

        this.actualizarLista();
        
    }

})