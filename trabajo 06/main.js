
const app = new Vue({
    el: '#app',
    data: {
        pokemones: [],
        getUrlPokemon:[],
        linkPokemon: [],
        pagActual: 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=12'
    },
    methods:{
        pagNext: function(){
            if (this.getUrlPokemon.next!=null) {
                this.pagActual = this.getUrlPokemon.next;
            this.pokemones = [];
            this.getUrlPokemon  = [];
            this.linkPokemon  = [];
            this.actualizarLista();
            }           
            
        },
        pagPrev: function(){
            if (this.getUrlPokemon.previous!=null) {
                this.pagActual = this.getUrlPokemon.previous;
                this.pokemones = [];
                this.getUrlPokemon  = [];
                this.linkPokemon  = [];
                this.actualizarLista(); 
            }           
            
        },
        actualizarLista: function (){

            axios.get(this.pagActual)
            .then(response => {
                this.getUrlPokemon = response.data;
                console.log(this.getUrlPokemon.results);
            })
            .then( () => {
                for (let item of this.getUrlPokemon.results) {
                    this.linkPokemon.push({
                        url: item.url
                    });
                }
                console.log(this.linkPokemon);
            })
            .then( () => {
                for (const item of this.linkPokemon) {
                    
                    axios.get(item.url)
                    .then(response => {
                        this.pokemones.push({
                            pokemon: response.data
                        });
                    });
                    
                }
                console.log(this.pokemones)
            })
            
        }
    },
    created: function() {

        this.actualizarLista();
        
    }

})